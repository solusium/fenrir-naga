# fenrir-naga

An implementation of the fenrir-shader-compiler (https://gitlab.com/solusium/fenrir-shader-compiler)
shader compiler abstraction layer based on naga (https://github.com/gfx-rs/wgpu/tree/trunk/naga).

use {
    abi_stable::{
        export_root_module,
        prefix_type::PrefixTypeTrait,
        rtry, sabi_extern_fn,
        sabi_trait::TD_Opaque,
        std_types::{
            RArc, ROption,
            RResult::{self, RErr, ROk},
            RSlice, RVec,
        },
        DynTrait,
    },
    bytemuck::try_cast_slice,
    core::{mem::take, str::from_utf8},
    fenrir_error::{invalid_argument, state_not_recoverable},
    fenrir_shader_compiler::{
        shader::Module as ShaderModule,
        stable_abi::{Module, ModuleRef},
        CompileError, Error, Inner, Language, Metadata, Stage,
    },
    naga::{
        compact::compact,
        proc::BoundsCheckPolicies,
        valid::{Capabilities, ModuleInfo, ValidationFlags, Validator},
        ShaderStage,
    },
    smallvec::{smallvec, SmallVec},
};

mod shader;

#[cfg(feature = "test")]
#[derive(Debug)]
pub struct Compiler {}

#[cfg(not(feature = "test"))]
#[derive(Debug)]
struct Compiler {}

#[inline]
#[export_root_module]
fn instantiate_root_module() -> ModuleRef {
    Module { new, compile, read }.leak_into_prefix()
}

#[inline]
#[sabi_extern_fn]
#[allow(improper_ctypes_definitions)]
fn new() -> DynTrait<'static, RArc<()>, Inner> {
    DynTrait::from_ptr(RArc::new(Compiler {}))
}

#[inline]
#[sabi_extern_fn]
#[allow(improper_ctypes_definitions)]
fn compile<'a>(
    inner: &DynTrait<'static, RArc<()>, Inner>,
    metadata: Metadata<'a>,
) -> RResult<RVec<u8>, Error> {
    let compiler = rtry!(match inner.downcast_as::<Compiler>() {
        Ok(compiler_) => ROk(compiler_),
        Err(_error) => RErr(Error::FenrirError(invalid_argument(
            "failed to cast {inner:?} to a fenrir_naga::Compiler: {_error}",
        ))),
    });

    match compiler.compile(metadata) {
        Err(error) => RErr(error),
        Ok(output) => ROk(output.into()),
    }
}

#[inline]
#[sabi_extern_fn]
#[allow(improper_ctypes_definitions)]
fn read(
    inner: &DynTrait<'static, RArc<()>, Inner>,
    source: RSlice<'_, u8>,
    language: Language,
    stage: ROption<Stage>,
) -> RResult<ShaderModule, Error> {
    let compiler = rtry!(match inner.downcast_as::<Compiler>() {
        Ok(compiler_) => ROk(compiler_),
        Err(_error) => RErr(Error::FenrirError(invalid_argument(
            "failed to cast {inner:?} to a fenrir_naga::Compiler: {_error}",
        ))),
    });

    compiler.read(source.into(), language, stage.into()).into()
}

impl Compiler {
    #[inline]
    fn compile(&self, metadata: Metadata) -> Result<Vec<u8>, Error> {
        let mut module = match metadata.source_language() {
            Language::GLSL => self.parse_glsl(metadata.stage(), metadata.source()),
            Language::SPIRV => self.parse_spirv(metadata.source()),
        }?;

        let validation_flags = ValidationFlags::all();

        let capabilities = Capabilities::all();

        {
            let validation_result =
                Validator::new(validation_flags, capabilities).validate(&module);

            let _ = match validation_result {
                Ok(module_info_) => Ok(module_info_),
                Err(error) => Err(Error::FenrirError(invalid_argument(&format!(
                    "validation errors: {error}"
                )))),
            }?;
        }

        compact(&mut module);

        let validation_result = Validator::new(validation_flags, capabilities).validate(&module);

        let module_info = match validation_result {
            Ok(module_info_) => Ok(module_info_),
            Err(error) => Err(Error::FenrirError(invalid_argument(&format!(
                "validation errors after compacting: {error}"
            )))),
        }?;

        match metadata.target_language() {
            Language::GLSL => self.write_glsl(&metadata, &module, &module_info),
            Language::SPIRV => self.write_spirv(&metadata, &module, &module_info),
        }
    }

    #[inline]
    #[cfg(feature = "test")]
    pub fn compile_test(&self, metadata: Metadata) -> Result<Vec<u8>, Error> {
        self.compile(metadata)
    }

    #[inline]
    fn get_naga_module(
        &self,
        source: &[u8],
        language: Language,
        stage: Option<Stage>,
    ) -> Result<naga::Module, Error> {
        match language {
            Language::GLSL => {
                const ALL_SHADER_STAGES: [Stage; 3] =
                    [Stage::Compute, Stage::Fragment, Stage::Vertex];

                let shader_stages: SmallVec<[Stage; 3]> = stage
                    .map_or(ALL_SHADER_STAGES.into_iter().collect(), |stage_| {
                        smallvec![stage_]
                    });

                let mut parse_results = shader_stages
                    .iter()
                    .map(|stage| self.parse_glsl(*stage, source))
                    .collect::<SmallVec<[_; 3]>>();

                let maybe_module = parse_results
                    .iter_mut()
                    .filter_map(|parse_result| parse_result.as_mut().ok())
                    .find(|_| true);

                match maybe_module {
                    Some(module_) => Ok(take(module_)),
                    None => {
                        let parse_results_strings = shader_stages
                            .into_iter()
                            .zip(parse_results)
                            .map(|(stage, parse_result)| format!("{stage:?}: {parse_result:?}"))
                            .collect::<SmallVec<[_; 3]>>();

                        Err(Error::FenrirError(invalid_argument(&format!
                ("couldn't parse source as glsl souce representing a shader stage: {parse_results_strings:?}"))))
                    }
                }
            }
            Language::SPIRV => self.parse_spirv(source),
        }
    }

    #[inline]
    fn parse_glsl(&self, stage: Stage, source: &[u8]) -> Result<naga::Module, Error> {
        let stage = match stage {
            Stage::Vertex => ShaderStage::Vertex,
            Stage::Compute => ShaderStage::Compute,
            Stage::Fragment => ShaderStage::Fragment,
        };

        let input = match from_utf8(source) {
            Ok(utf8_source) => Ok(utf8_source),
            Err(error) => Err(Error::FenrirError(invalid_argument(&format!(
                "source is not valid utf-8: {error}"
            )))),
        }?;

        let defines = Default::default();

        let parse_result = naga::front::glsl::Frontend::default()
            .parse(&naga::front::glsl::Options { stage, defines }, input);

        match parse_result {
            Ok(module) => Ok(module),
            Err(error) => Err(Error::CompileError(
                error
                    .errors
                    .iter()
                    .map(|error| CompileError::new(error.meta.to_range(), &error.kind.to_string()))
                    .collect(),
            )),
        }
    }

    #[inline]
    fn parse_spirv(&self, source: &[u8]) -> Result<naga::Module, Error> {
        let parse_result =
            naga::front::spv::parse_u8_slice(source, &naga::front::spv::Options::default());

        match parse_result {
            Ok(module) => Ok(module),
            Err(_error) => Err(Error::FenrirError(invalid_argument(
                "error in SPIR-V source: {_error}",
            ))),
        }
    }

    #[inline]
    fn read(
        &self,
        source: &[u8],
        language: Language,
        stage: Option<Stage>,
    ) -> Result<ShaderModule, Error> {
        Ok(ShaderModule::new(
            fenrir_shader_compiler::shader::stable_abi::Trait_TO::from_value(
                crate::shader::Module::new(self.get_naga_module(source, language, stage)?),
                TD_Opaque,
            ),
        ))
    }

    #[inline]
    #[cfg(feature = "test")]
    pub fn read_test(
        &self,
        source: &[u8],
        language: Language,
        stage: Option<Stage>,
    ) -> Result<crate::shader::Module, Error> {
        Ok(crate::shader::Module::new(
            self.get_naga_module(source, language, stage)?,
        ))
    }

    #[inline]
    fn write_glsl(
        &self,
        metadata: &Metadata,
        module: &naga::Module,
        module_info: &ModuleInfo,
    ) -> Result<Vec<u8>, Error> {
        let mut output = String::new();

        let options = naga::back::glsl::Options::default();

        let shader_stage = match metadata.stage() {
            Stage::Vertex => ShaderStage::Vertex,
            Stage::Compute => ShaderStage::Compute,
            Stage::Fragment => ShaderStage::Fragment,
        };

        let entry_point = "main".to_string();

        let multiview = None;

        let pipeline_options = naga::back::glsl::PipelineOptions {
            shader_stage,
            entry_point,
            multiview,
        };

        let policies = BoundsCheckPolicies::default();

        let writer_result = naga::back::glsl::Writer::new(
            &mut output,
            module,
            module_info,
            &options,
            &pipeline_options,
            policies,
        );

        match writer_result {
            Err(error) => Err(Error::FenrirError(state_not_recoverable(&format!(
                "couldn't write GLSL: {error}"
            )))),
            Ok(mut writer) => match writer.write() {
                Err(error) => Err(Error::FenrirError(state_not_recoverable(&format!(
                    "failed to write GLSL: {error}"
                )))),
                Ok(_) => Ok(output.into_bytes()),
            },
        }
    }

    #[inline]
    fn write_spirv(
        &self,
        metadata: &Metadata,
        module: &naga::Module,
        module_info: &ModuleInfo,
    ) -> Result<Vec<u8>, Error> {
        let options = naga::back::spv::Options::default();

        let shader_stage = match metadata.stage() {
            Stage::Vertex => ShaderStage::Vertex,
            Stage::Compute => ShaderStage::Compute,
            Stage::Fragment => ShaderStage::Fragment,
        };

        let entry_point = "main".to_string();

        let pipeline_options_inner = naga::back::spv::PipelineOptions {
            shader_stage,
            entry_point,
        };

        let pipeline_options = Some(&pipeline_options_inner);

        let write_result =
            naga::back::spv::write_vec(module, module_info, &options, pipeline_options);

        match write_result {
            Err(error) => Err(Error::FenrirError(state_not_recoverable(&format!(
                "failed to write SPIR-V: {error}"
            )))),
            Ok(spirv) => match try_cast_slice::<u32, u8>(&spirv) {
                Err(error) => Err(Error::FenrirError(state_not_recoverable(&format!(
                    "failed to cast SPIR-V to u8: {error}"
                )))),
                Ok(output) => Ok(output.to_vec()),
            },
        }
    }
}

impl Default for Compiler {
    #[inline]
    fn default() -> Self {
        Self {}
    }
}

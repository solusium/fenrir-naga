use {
    abi_stable::std_types::{RResult, RVec},
    fenrir_error::{result_out_of_range, Error},
    fenrir_shader_compiler::{shader::stable_abi::Trait, EntryPoint, Stage},
    naga::{Block, Function, RayQueryFunction, ShaderStage, Statement, SwitchCase},
    std::ops::Range,
};

#[cfg(feature = "test")]
#[derive(Debug)]
pub struct Module {
    inner: naga::Module,
}

#[cfg(not(feature = "test"))]
#[derive(Debug)]
pub(super) struct Module {
    inner: naga::Module,
}

impl Module {
    #[inline]
    #[must_use]
    pub(super) fn new(inner: naga::Module) -> Self {
        Self { inner }
    }

    #[inline]
    fn compare_blocks(left: &Block, right: &Block) -> bool {
        Self::compare_iteratables(left.iter(), right.iter(), Self::compare_statements)
    }

    #[inline]
    fn compare_functions(function: &Function, other_function: &Function) -> bool {
        function.name == other_function.name
            && Self::compare_iteratables(
                function.arguments.iter(),
                other_function.arguments.iter(),
                |(argument, other_argument)| {
                    argument.name == other_argument.name
                        && argument.ty == other_argument.ty
                        && argument.binding == other_argument.binding
                },
            )
            && match (function.result.as_ref(), other_function.result.as_ref()) {
                (Some(result), Some(other_result)) => {
                    result.ty == other_result.ty && result.binding == other_result.binding
                }
                (None, None) => true,
                _ => false,
            }
            && Self::compare_iteratables(
                function.local_variables.iter(),
                other_function.local_variables.iter(),
                |((handle, local_variable), (other_handle, other_local_variable))| {
                    handle == other_handle
                        && Self::compare_optionals(&local_variable.name, &other_local_variable.name)
                        && local_variable.ty == other_local_variable.ty
                        && Self::compare_optionals(&local_variable.init, &other_local_variable.init)
                },
            )
            && Self::compare_iteratables(
                function.expressions.iter(),
                other_function.expressions.iter(),
                |((handle, expression), (other_handle, other_expression))| {
                    handle == other_handle && expression == other_expression
                },
            )
            && function.named_expressions == other_function.named_expressions
            && Self::compare_iteratables(
                function.body.iter(),
                other_function.body.iter(),
                Self::compare_statements,
            )
    }

    #[inline]
    fn compare_iteratables<T, I: Iterator<Item = T>>(
        left: I,
        right: I,
        f: impl Fn((T, T)) -> bool,
    ) -> bool {
        left.zip(right).all(f)
    }

    #[inline]
    fn compare_optionals<T: PartialEq>(maybe_left: &Option<T>, maybe_right: &Option<T>) -> bool {
        match (maybe_left.as_ref(), maybe_right.as_ref()) {
            (Some(left), Some(right)) => left == right,
            (None, None) => true,
            _ => false,
        }
    }

    #[inline]
    fn compare_switch_cases((switch_case, other_switch_case): (&SwitchCase, &SwitchCase)) -> bool {
        switch_case.value == other_switch_case.value
            && Self::compare_iteratables(
                switch_case.body.iter(),
                other_switch_case.body.iter(),
                Self::compare_statements,
            )
            && switch_case.fall_through == other_switch_case.fall_through
    }

    fn compare_statements(arguments: (&Statement, &Statement)) -> bool {
        match arguments {
            (
                Statement::Atomic {
                    pointer,
                    fun,
                    value,
                    result,
                },
                other_statement,
            ) => {
                let other_pointer = pointer;
                let other_fun = fun;
                let other_value = value;
                let other_result = result;

                match other_statement {
                    Statement::Atomic {
                        pointer,
                        fun,
                        value,
                        result,
                    } => {
                        pointer == other_pointer
                            && fun == other_fun
                            && value == other_value
                            && result == other_result
                    }
                    _ => false,
                }
            }
            (Statement::Barrier(barrier), Statement::Barrier(other_barrier)) => {
                barrier == other_barrier
            }
            (Statement::Block(block), Statement::Block(other_block)) => {
                Self::compare_blocks(block, other_block)
            }
            (Statement::Break, Statement::Break) => true,
            (
                Statement::Call {
                    function,
                    arguments,
                    result,
                },
                other_statement,
            ) => {
                let other_function = function;
                let other_arguments = arguments;
                let other_result = result;

                match other_statement {
                    Statement::Call {
                        function,
                        arguments,
                        result,
                    } => {
                        function == other_function
                            && arguments == other_arguments
                            && result == other_result
                    }
                    _ => false,
                }
            }
            (Statement::Continue, Statement::Continue) => true,
            (Statement::Emit(range), Statement::Emit(other_range)) => Self::compare_iteratables(
                range.clone(),
                other_range.clone(),
                |(expression, other_expression)| expression == other_expression,
            ),
            (
                Statement::If {
                    condition,
                    accept,
                    reject,
                },
                other_statement,
            ) => {
                let other_condition = condition;
                let other_accept = accept;
                let other_reject = reject;

                match other_statement {
                    Statement::If {
                        condition,
                        accept,
                        reject,
                    } => {
                        condition == other_condition
                            && Self::compare_blocks(accept, other_accept)
                            && Self::compare_blocks(reject, other_reject)
                    }
                    _ => false,
                }
            }
            (
                Statement::ImageStore {
                    image,
                    coordinate,
                    array_index,
                    value,
                },
                other_statement,
            ) => {
                let other_image = image;
                let other_coordinate = coordinate;
                let other_array_index = array_index;
                let other_value = value;

                match other_statement {
                    Statement::ImageStore {
                        image,
                        coordinate,
                        array_index,
                        value,
                    } => {
                        image == other_image
                            && coordinate == other_coordinate
                            && array_index == other_array_index
                            && value == other_value
                    }
                    _ => false,
                }
            }
            (Statement::Kill, Statement::Kill) => true,
            (
                Statement::Loop {
                    body,
                    continuing,
                    break_if,
                },
                other_statement,
            ) => {
                let other_body = body;
                let other_continuing = continuing;
                let other_break_if = break_if;

                match other_statement {
                    Statement::Loop {
                        body,
                        continuing,
                        break_if,
                    } => {
                        Self::compare_blocks(body, other_body)
                            && Self::compare_blocks(continuing, other_continuing)
                            && break_if == other_break_if
                    }
                    _ => false,
                }
            }
            (Statement::RayQuery { query, fun }, other_statement) => {
                let other_query = query;
                let other_fun = fun;

                match other_statement {
                    Statement::RayQuery { query, fun } => {
                        query == other_query
                            && match (fun, other_fun) {
                                (
                                    RayQueryFunction::Initialize {
                                        acceleration_structure,
                                        descriptor,
                                    },
                                    other_ray_query_function,
                                ) => {
                                    let other_acceleration_structure = acceleration_structure;
                                    let other_descriptor = descriptor;

                                    match other_ray_query_function {
                                        RayQueryFunction::Initialize {
                                            acceleration_structure,
                                            descriptor,
                                        } => {
                                            acceleration_structure == other_acceleration_structure
                                                && descriptor == other_descriptor
                                        }
                                        _ => false,
                                    }
                                }
                                (
                                    RayQueryFunction::Proceed { result },
                                    other_ray_query_function,
                                ) => {
                                    let other_result = result;

                                    match other_ray_query_function {
                                        RayQueryFunction::Proceed { result } => {
                                            result == other_result
                                        }
                                        _ => false,
                                    }
                                }
                                (RayQueryFunction::Terminate, RayQueryFunction::Terminate) => true,
                                _ => false,
                            }
                    }
                    _ => false,
                }
            }
            (Statement::Return { value }, other_statement) => {
                let other_value = value;

                match other_statement {
                    Statement::Return { value } => value == other_value,
                    _ => false,
                }
            }
            (Statement::Store { pointer, value }, other_statement) => {
                let other_pointer = pointer;
                let other_value = value;

                match other_statement {
                    Statement::Store { pointer, value } => {
                        pointer == other_pointer && value == other_value
                    }
                    _ => false,
                }
            }
            (Statement::Switch { selector, cases }, other_statement) => {
                let other_selector = selector;
                let other_cases = cases;

                match other_statement {
                    Statement::Switch { selector, cases } => {
                        selector == other_selector
                            && Self::compare_iteratables(
                                cases.iter(),
                                other_cases.iter(),
                                Self::compare_switch_cases,
                            )
                    }
                    _ => false,
                }
            }
            (Statement::WorkGroupUniformLoad { pointer, result }, other_statement) => {
                let other_pointer = pointer;
                let other_result = result;

                match other_statement {
                    Statement::WorkGroupUniformLoad { pointer, result } => {
                        pointer == other_pointer && result == other_result
                    }
                    _ => false,
                }
            }
            _ => false,
        }
    }

    #[inline]
    fn entry_points(&self) -> Result<RVec<EntryPoint<'_>>, Error> {
        self.inner
            .entry_points
            .iter()
            .map(|entry_point| {
                let stage = match entry_point.stage {
                    ShaderStage::Vertex => Stage::Vertex,
                    ShaderStage::Fragment => Stage::Fragment,
                    ShaderStage::Compute => Stage::Compute,
                };

                let cast = |entry: u32| match entry.try_into() {
                    Ok(entry) => Ok(entry),
                    Err(_) => Err(result_out_of_range(
                        Range {
                            start: 0.into(),
                            end: usize::MAX.into(),
                        },
                        entry.into(),
                    )),
                };

                let workgroup_size = [
                    cast(entry_point.workgroup_size[0])?,
                    cast(entry_point.workgroup_size[1])?,
                    cast(entry_point.workgroup_size[2])?,
                ];

                Ok(EntryPoint::new(&entry_point.name, stage, workgroup_size))
            })
            .collect::<Result<RVec<EntryPoint<'_>>, Error>>()
    }

    #[inline]
    #[cfg(feature = "test")]
    pub fn entry_tests_test(&self) -> Result<RVec<EntryPoint<'_>>, Error> {
        self.entry_points()
    }
}

impl Eq for Module {}

impl PartialEq for Module {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.inner.types.iter().zip(other.inner.types.iter()).all(
            |((handle, data_type), (other_handle, other_data_type))| {
                handle == other_handle && data_type == other_data_type
            },
        ) && self.inner.special_types.ray_desc == other.inner.special_types.ray_desc
            && self.inner.special_types.ray_intersection
                == other.inner.special_types.ray_intersection
            && Self::compare_iteratables(
                self.inner.special_types.predeclared_types.iter(),
                other.inner.special_types.predeclared_types.iter(),
                |(predeclared_type, other_predeclared_type)| {
                    predeclared_type == other_predeclared_type
                },
            )
            && Self::compare_iteratables(
                self.inner.constants.iter(),
                other.inner.constants.iter(),
                |(constant, other_constant)| constant == other_constant,
            )
            && Self::compare_iteratables(
                self.inner.global_variables.iter(),
                other.inner.global_variables.iter(),
                |(global_variable, other_global_variable)| global_variable == other_global_variable,
            )
            && Self::compare_iteratables(
                self.inner.functions.iter(),
                other.inner.functions.iter(),
                |((handle, function), (other_handle, other_function))| {
                    handle == other_handle && Self::compare_functions(function, other_function)
                },
            )
            && Self::compare_iteratables(
                self.inner.entry_points.iter(),
                other.inner.entry_points.iter(),
                |(entry_point, other_entry_point)| {
                    entry_point.name == other_entry_point.name
                        && entry_point.stage == other_entry_point.stage
                        && entry_point.early_depth_test == other_entry_point.early_depth_test
                        && entry_point.workgroup_size == other_entry_point.workgroup_size
                        && Self::compare_functions(
                            &entry_point.function,
                            &other_entry_point.function,
                        )
                },
            )
    }
}

impl Trait for Module {
    #[inline]
    fn entry_points(&self) -> RResult<RVec<EntryPoint<'_>>, Error> {
        self.entry_points().into()
    }
}

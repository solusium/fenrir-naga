use {
    fenrir_naga::Compiler,
    fenrir_shader_compiler::{Language, Metadata, Stage},
    pretty_assertions::assert_eq,
};

#[test]
fn glsl_to_spirv() {
    let compiler = Compiler::default();

    let glsl = "#version 450\n\
        \n\
        vec2 positions[3] = vec2[](\n\
        vec2(0.0, -0.5),\n\
        vec2(0.5, 0.5),\n\
        vec2(-0.5, 0.5)\n\
        );\n\
        \n\
        void main() {\n\
            gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);\n\
        }\n";

    {
        let glsl_module_result =
            compiler.read_test(glsl.as_bytes(), Language::GLSL, Some(Stage::Vertex));

        assert!(glsl_module_result.is_ok());

        let glsl_module = glsl_module_result.unwrap();

        let entry_points_result = glsl_module.entry_tests_test();

        assert!(entry_points_result.is_ok());

        let entry_points = entry_points_result.unwrap();

        assert_eq!(1, entry_points.len());

        let entry_point = &entry_points[0];

        assert_eq!("main", entry_point.name());
        assert_eq!(Stage::Vertex, entry_point.stage());
    }

    let compile_result = compiler.compile_test(
        Metadata::build(
            glsl.as_bytes(),
            Language::GLSL,
            Language::SPIRV,
            Stage::Vertex,
        )
        .build(),
    );

    assert!(compile_result.is_ok());

    let spirv = compile_result.unwrap();

    {
        let spirv_module_result = compiler.read_test(&spirv, Language::SPIRV, Some(Stage::Vertex));

        assert!(spirv_module_result.is_ok());

        let spirv_module = spirv_module_result.unwrap();

        let entry_points_result = spirv_module.entry_tests_test();

        assert!(entry_points_result.is_ok());

        let entry_points = entry_points_result.unwrap();

        assert_eq!(1, entry_points.len());

        let entry_point = &entry_points[0];

        assert_eq!("main", entry_point.name());
        assert_eq!(Stage::Vertex, entry_point.stage());
    }

    let parse_result =
        naga::front::spv::parse_u8_slice(&spirv, &naga::front::spv::Options::default());

    assert!(parse_result.is_ok());

    let module = parse_result.unwrap();

    assert_eq!(1, module.entry_points.len());
    assert_eq!("main", module.entry_points[0].name);

    ["positions", "gl_Position", "gl_VertexIndex"]
        .into_iter()
        .for_each(|global_variable_name| {
            let global_variable = module.global_variables.iter().find(|(_, global_variable)| {
                global_variable
                    .name
                    .as_ref()
                    .map_or(false, |name| global_variable_name == name)
            });

            assert!(global_variable.is_some());
        });
}
